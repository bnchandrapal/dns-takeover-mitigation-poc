import boto3
import whois
import time
from tabulate import tabulate
from netaddr import IPAddress
import csv

region = 'us-east-1'
session = boto3.Session(
                region_name='us-east-1',
                profile_name='default'
)

# Store all records
a_records = {}
cname_records = {}
mx_records = {}
ns_records = {}

# Store the whitelist
a_whitelist = {}
cname_whitelist = {}
mx_whitelist = {}
ns_whitelist = {}

# Store cloudfront domains
cloudfront_domains = set()
s3_domains = set()
elb_public_domains = set()
elb_private_domains = set()

# Get all IPs
ec2_instance_ips = set()
ec2_elastic_ips = set()
ec2_nat_ips = set()
ec2_network_interface = set()

# Store the violations
mx_violations = {}
a_violations = {}
ns_violations = {}
cname_violations = {}

all_ips = set()
all_services = set()

def print_table(violation, header):
    tmp = []
    for record in violation:
        for value in violation[record]:
            tmp.append([record, value])
    print(tabulate(tmp, header, tablefmt='psql'))
    print()

def parse_whitelist_csv(filename, result):
    reader = csv.reader(open(filename, 'r'))
    for row in reader:
        if row[0] not in result.keys():
            result.update({ row[0] : [ row[1] ]})
        else:
            tmp = result[row[0]]
            tmp.append(row[1])
            result[row[0]] = tmp

def whitelisted(whitelist, domain, address):
    for record in whitelist.keys():
        if record == domain:
            for value in whitelist[record]:
                if value == address:
                    return True
    return False

def get_domains():
    client = session.client('route53')
    zones = client.list_hosted_zones()
    paginator = client.get_paginator('list_resource_record_sets')
    for zone in zones.get('HostedZones'):
        zoneId = zone.get('Id')
        source_zone_records = paginator.paginate(HostedZoneId=zoneId)
        for record_set in source_zone_records:
            for record in record_set['ResourceRecordSets']:
                if record.get('ResourceRecords') is not None and '\\052' not in record['Name']:
                    # Collect A records
                    if record['Type'] == 'A':
                        tmp = []
                        for ip in record['ResourceRecords']:
                            if not IPAddress(ip['Value']).is_private():
                                tmp.append(ip['Value'])
                        a_records.update({ record['Name'].strip('.') : tmp })
                    # Collect CNAME records
                    elif record['Type'] == 'CNAME':
                        cname_records.update({ record['Name'].strip('.') : record['ResourceRecords'][0]['Value'] })
                    # Collect MX records priority
                    elif record['Type'] == 'MX':
                        tmp = []
                        for mx in record['ResourceRecords']:
                            tmp.append(mx['Value'].split()[1].lower())
                        mx_records.update({ record['Name'].strip('.') : tmp})
                    # Collect NS records
                    elif record['Type'] == 'NS':
                        tmp = []
                        for ns in record['ResourceRecords']:
                            tmp.append(ns['Value'])
                        ns_records.update({ record['Name'].strip('.') : tmp })

def get_cloudfront_resources():
    client = session.client('cloudfront')

    # List distributions
    dists = client.list_distributions()
    if dists.get('DistributionList') is not None:
        for distribution in dists['DistributionList']['Items']:
            if distribution.get('DomainName') is not None:
                cloudfront_domains.add(distribution['DomainName'])

    # List streaming distributions
    stream = client.list_streaming_distributions()
    if stream.get('DistributionList') is not None:
        for distribution in stream['StreamingDistributionList']['Items']:
            if distribution.get('DomainName') is not None:
                cloudfront_domains.add(distribution['DomainName'])

def get_s3_buckets():
    client = session.client('s3')
    response = client.list_buckets()
    for bucket in response['Buckets']:
        # S3 url
        s3_domains.add(bucket['Name'] + '.s3.amazonaws.com')
        s3_domains.add(bucket['Name'] + '.s3.' + region + '.amazonaws.com')
        # S3 static website url
        s3_domains.add(bucket['Name'] + '.s3-website.' + region + '.amazonaws.com')
        s3_domains.add(bucket['Name'] + '.s3-website-' + region + '.amazonaws.com')

def get_elb():
    client = session.client('elb')
    load_balancers = client.describe_load_balancers()
    for elb in load_balancers['LoadBalancerDescriptions']:
        # Internal ELB
        if elb['Scheme'] == 'internal':
            elb_private_domains.add(elb['DNSName'])
        # Public ELB
        elif elb['Scheme'] == 'internet-facing':
            elb_public_domains.add(elb['DNSName'])

def get_aws_ips():
    client = session.client('ec2')
    # EC2 instances
    reservation_list = client.describe_instances()
    for reservation in reservation_list.get('Reservations'):
        for instance in reservation.get('Instances'):
            ip = instance.get('PublicIpAddress')
            if ip is not None:
                ec2_instance_ips.add(ip)
    
    # NAT Gateways
    nat_gateways = client.describe_nat_gateways()
    for item in nat_gateways.get('NatGateways'):
        for data in item.get('NatGatewayAddresses'):
            ip = data.get('PublicIp')
            if ip is not None:
                ec2_nat_ips.add(ip)
    
    # Elastic IPs
    elastic_instances = client.describe_addresses()
    for item in elastic_instances.get('Addresses'):
        ip = item.get('PublicIp')
        if ip is not None:
            ec2_elastic_ips.add(ip)
    
    # Elastic Network Interfaces
    network_instances = client.describe_network_interfaces()
    for item in network_instances.get('NetworkInterfaces'):
        if item.get('Association') is not None:
            ip = item.get('Association').get('PublicIp')
            if ip is not None:
                ec2_network_interface.add(ip)


def main():
    
    # 1. Get all DNS records and store it
    get_domains()

    # 2. Get all resources that you own (IPs and CNAME)
    get_cloudfront_resources()
    get_s3_buckets()
    get_elb()
    get_aws_ips()

    all_services = cloudfront_domains | s3_domains | elb_public_domains | elb_private_domains | set(a_records.keys()) | set(cname_records.keys())
    all_ips = ec2_elastic_ips | ec2_instance_ips | ec2_nat_ips | ec2_network_interface

    parse_whitelist_csv("a_whitelist.csv", a_whitelist)
    parse_whitelist_csv("cname_whitelist.csv", cname_whitelist)
    parse_whitelist_csv("mx_whitelist.csv", mx_whitelist)
    parse_whitelist_csv("ns_whitelist.csv", ns_whitelist)

    # 3. Check if the DNS records points to our resources. If not check them in whitelisted domains & IPs. If not, then report it.
    for record in a_records.keys():
        for ip in a_records[record]:
            if ip not in all_ips and not whitelisted(a_whitelist, record, ip):
                if record not in a_violations.keys():
                    a_violations.update({ record : [ ip ]})
                else:
                    tmp = a_violations[record]
                    tmp.append(ip)
                    a_violations[record] = tmp

    print("A record violations: ")
    print_table(a_violations, [ "A record", "IP address"])

    for x in cname_records.keys():
        if cname_records[x].strip('.') not in all_services and not whitelisted(cname_whitelist, x, cname_records[x]):
            #print(x + " = " + cname_records[x])
            cname_violations.update({ x : [ cname_records[x] ]})
    
    print("CNAME record violations: ")
    print_table(cname_violations, [ "CNAME record", "Value"])

    for record in mx_records.keys():
        for mx in mx_records[record]:
            if mx.strip('.') not in all_services and not whitelisted(mx_whitelist, record, mx.strip('.')):
                # Its a violation
                if record not in mx_violations.keys():
                    mx_violations.update({ record : [ mx.strip('.') ]})
                else:
                    tmp = mx_violations[record]
                    tmp.append(mx.strip('.'))
                    mx_violations[record] = tmp

    print("MX record violations: ")
    print_table(mx_violations, [ "MX record", "Value"])

    for record in ns_records.keys():
        for ns in ns_records[record]:
            if ns.strip('.') not in all_services and not whitelisted(ns_whitelist, record, ns.strip('.')):
                if record not in ns_violations.keys():
                    ns_violations.update({ record : [ ns.strip('.') ] })
                else:
                    tmp = ns_violations[record]
                    tmp.append(ns.strip('.'))
                    ns_violations[record] = tmp

    print("NS record violations: ")
    print_table(ns_violations, [ "NS record", "Value"])

if __name__ == "__main__":
    main()